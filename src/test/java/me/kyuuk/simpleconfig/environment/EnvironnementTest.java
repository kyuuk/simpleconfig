/**
 * 
 */
package me.kyuuk.simpleconfig.environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import me.kyuuk.simpleconfig.exceptions.InvalidEnvironment;
import me.kyuuk.simpleconfig.exceptions.InvalidKey;
import me.kyuuk.simpleconfig.exceptions.PropertyNotFound;

/**
 * @author kyuk
 *
 */
public class EnvironnementTest extends AbstractEnvironmentTest
{

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#EnvironmentUtils(java.lang.String)}.
	 * 
	 * @throws InvalidEnvironment
	 */
	@Test(expected = InvalidEnvironment.class)
	public void testEnvironmentUtilsEnumNotFound() throws InvalidEnvironment
	{
		env = new Environment("toto");
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#EnvironmentUtils(java.lang.String)}.
	 * 
	 * @throws InvalidEnvironment
	 */
	@Test()
	public void testEnvironmentUtilsfileNotFound() throws InvalidEnvironment
	{
		new Environment("PROD");
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#getProperty(java.lang.String)}.
	 * 
	 * @throws InvalidKey
	 */
	@Test
	public void testGetProperty() throws InvalidKey
	{
		String test = env.getStringProperty("Prop");
		assertNotNull(test);
		assertEquals("property", test);
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#getProperty(java.lang.String)}.
	 * 
	 * @throws InvalidKey
	 */
	@Test(expected = InvalidKey.class)
	public void testGetPropertyNull() throws InvalidKey
	{
		env.getStringProperty(null);
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#getBooleanProperty(java.lang.String)}.
	 * 
	 * @throws PropertyNotFound
	 * @throws InvalidKey
	 */
	@Test
	public void testGetBooleanProperty() throws InvalidKey, PropertyNotFound
	{
		Boolean test = env.getBooleanProperty("Bool");
		assertFalse(test);
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#getBooleanProperty(java.lang.String)}.
	 * 
	 * @throws PropertyNotFound
	 * @throws InvalidKey
	 */
	@Test(expected = InvalidKey.class)
	public void testGetBooleanPropertyNullKey() throws InvalidKey, PropertyNotFound
	{
		env.getBooleanProperty(null);
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#getBooleanProperty(java.lang.String)}.
	 * 
	 * @throws PropertyNotFound
	 * @throws InvalidKey
	 */
	@Test(expected = InvalidKey.class)
	public void testGetBooleanPropertyEmptyKey() throws InvalidKey, PropertyNotFound
	{
		Boolean test = env.getBooleanProperty("");
		assertFalse(test);
	}

	@Test
	public void testGetBooleanPropertyInvalidValue() throws InvalidKey, PropertyNotFound
	{
		boolean test = env.getBooleanProperty("InvalidBool");
		assertFalse(test);
	}

	@Test(expected = InvalidKey.class)
	public void testGetIntPropertyEmptyKey() throws InvalidKey, PropertyNotFound
	{
		env.getIntProperty("");
	}

	@Test(expected = InvalidKey.class)
	public void testGetIntPropertyNullKey() throws InvalidKey, PropertyNotFound
	{
		env.getIntProperty(null);
	}

	@Test(expected = PropertyNotFound.class)
	public void testGetIntPropertyEmptyValue() throws InvalidKey, PropertyNotFound
	{
		env.getIntProperty("Empty");
	}

	@Test(expected = PropertyNotFound.class)
	public void testGetIntPropertyInvalidValue() throws InvalidKey, PropertyNotFound
	{
		env.getIntProperty("InvalidInt");
	}

	@Test
	public void testGetIntProperty() throws InvalidKey, PropertyNotFound
	{
		int test = env.getIntProperty("Int");
		assertEquals(25, test);
	}

}
