/**
 * 
 */
package me.kyuuk.simpleconfig.environment;

import static org.junit.Assert.assertNotNull;

import org.junit.BeforeClass;

import me.kyuuk.simpleconfig.exceptions.InvalidEnvironment;

/**
 * @author kyuk
 *
 */
public abstract class AbstractEnvironmentTest
{
	protected static Environment env;

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#EnvironmentUtils(java.lang.String)}.
	 * 
	 * @throws InvalidEnvironment
	 */
	@BeforeClass
	public static void testEnvironmentUtils() throws InvalidEnvironment
	{
		env = new Environment("TEST");
		assertNotNull(env);
	}
}
