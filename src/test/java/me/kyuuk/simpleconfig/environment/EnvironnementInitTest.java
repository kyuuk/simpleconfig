/**
 * 
 */
package me.kyuuk.simpleconfig.environment;

import org.junit.Test;

import me.kyuuk.simpleconfig.exceptions.InvalidEnvironment;

/**
 * @author kyuk
 *
 */
public class EnvironnementInitTest
{

	@Test(expected = InvalidEnvironment.class)
	public void testEnvironmentEnumNotFound() throws InvalidEnvironment
	{
		new Environment("toto");
	}

	@Test()
	public void testEnvironmentfileNotFound() throws InvalidEnvironment
	{
		new Environment("PROD");
	}

	/**
	 * Test method for
	 * {@link me.kyuuk.trickstock.config.environment.EnvironmentUtils#EnvironmentUtils(java.lang.String)}.
	 * 
	 * @throws InvalidEnvironment
	 */
	@Test()
	public void testEnvironmentEmptyEnv() throws InvalidEnvironment
	{
		new Environment("");
	}

}
