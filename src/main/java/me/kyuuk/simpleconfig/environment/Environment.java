package me.kyuuk.simpleconfig.environment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.kyuuk.simpleconfig.exceptions.InvalidEnvironment;
import me.kyuuk.simpleconfig.exceptions.InvalidKey;
import me.kyuuk.simpleconfig.exceptions.PropertyNotFound;

public class Environment
{
	private static final Logger logger = LogManager.getLogger(Environment.class);
	private static final String PATCH_STR = "$ENV$";
	private static final String PATCHABLE_PROP_FILE = "App_$ENV$.properties";
	private static String Propertiesfile;
	private static Properties props;
	private static EnvTypes ENV;

	/**
	 * initialize configuration for the environment parameter env
	 * 
	 * @param env Name of the current environment
	 * @throws InvalidEnvironment if the parameter env is not contained in the
	 *                            {@link EnvTypes} envTypes ENUM
	 */
	public Environment(String env) throws InvalidEnvironment
	{
		setEnvironment(env);
		logger.info("Intializing application with Environment [{}]", ENV);
		loadProps();
	}

	private void setEnvironment(String env) throws InvalidEnvironment
	{
		if (StringUtils.isBlank(env))
		{
			ENV = EnvTypes.PROD;
		} else
		{
			try
			{
				ENV = EnvTypes.valueOf(env);
			} catch (IllegalArgumentException e)
			{
				throw new InvalidEnvironment();
			}
		}
	}

	private static void loadProps()
	{
		if (props == null)
		{
			Propertiesfile = PATCHABLE_PROP_FILE.replace(PATCH_STR, ENV.getname());

			InputStream input = null;
			try
			{
				input = Environment.class.getClassLoader().getResourceAsStream(Propertiesfile);
				if (input == null)
				{
					throw new FileNotFoundException("Properties file [" + Propertiesfile + "] not found");
				}
				props = new Properties();
				props.load(input);
			} catch (IOException e)
			{
				logger.fatal("Unable to read properties file [{}] {}", Propertiesfile, e);
				props = null;
			} finally
			{
				if (input != null)
				{
					try
					{
						input.close();
					} catch (IOException e)
					{
						logger.fatal("Unable to close inputSteam for file [{}] {}", Propertiesfile, e);
					}
				}
			}
		}
	}

	/**
	 * Get the value of the key in parameter as a String
	 * 
	 * @param key the name of the property
	 * @return A String value of the property
	 * @throws InvalidKey if the parameter key is empty
	 */
	public String getStringProperty(String key) throws InvalidKey
	{
		if (StringUtils.isBlank(key))
		{
			throw new InvalidKey("Key [" + key + "] is Invalid");
		}
		String result = props.getProperty(key);
		logger.info("Got value [{}] for key [{}] from properties File [{}]", result, key, Propertiesfile);
		return result;
	}

	/**
	 * Get the value of the key in parameter parsed to a boolean true if the
	 * property is not empty and equals ignore case true false otherwise
	 * 
	 * @param key the name of the property
	 * @return A boolean value of the property
	 * @throws PropertyNotFound if the property is empty or does not exist
	 * @throws InvalidKey       if the parameter key is empty
	 */
	public boolean getBooleanProperty(String key) throws InvalidKey, PropertyNotFound
	{
		String Property = getStringProperty(key);
		return Boolean.valueOf(Property);
	}

	/**
	 * Get the value of the key in parameter parsed to an integer
	 * 
	 * @param key the name of the property
	 * @return An int value of the property
	 * @throws PropertyNotFound if the property is empty, does not exist or is not a
	 *                          number
	 * @throws InvalidKey       if the parameter key is empty
	 */
	public int getIntProperty(String key) throws PropertyNotFound, InvalidKey
	{
		String Property = getStringProperty(key);
		if (!StringUtils.isNumeric(Property))
		{
			logger.error("Value [{}] for key [{}] is not a number", Property, key);
			throw new PropertyNotFound("Value [" + Property + "] for key [" + key + "] is not a number");
		}
		return Integer.valueOf(Property);
	}
}
