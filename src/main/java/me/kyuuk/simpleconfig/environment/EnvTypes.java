package me.kyuuk.simpleconfig.environment;

public enum EnvTypes
{
	TEST("TEST"),
	DEV("DEV"),
	INT("INT"),
	PROD("PROD");

	private final String name;

	EnvTypes(String name)
	{
		this.name = name;
	}

	public String getname()
	{
		return this.name;
	}
}
