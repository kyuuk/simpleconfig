/**
 * 
 */
package me.kyuuk.simpleconfig.exceptions;

/**
 * @author kyuk
 *
 */
public class PropertyNotFound extends Exception
{
	private static final long serialVersionUID = 1L;

	public PropertyNotFound()
	{
		super();
	}

	public PropertyNotFound(String message)
	{
		super(message);
	}
}
