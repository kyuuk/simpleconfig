package me.kyuuk.simpleconfig.exceptions;

public class InvalidEnvironment extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidEnvironment()
	{
		super();
	}

	public InvalidEnvironment(Throwable t)
	{
		super(t);
	}

	public InvalidEnvironment(String message)
	{
		super(message);
	}

}
