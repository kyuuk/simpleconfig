/**
 * 
 */
package me.kyuuk.simpleconfig.exceptions;

/**
 * @author kyuk
 *
 */
public class InvalidKey extends Exception
{
	private static final long serialVersionUID = 1L;

	public InvalidKey()
	{
		super();
	}

	public InvalidKey(String message)
	{
		super(message);
	}
}
